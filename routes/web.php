<?php

use App\Http\Controllers\WebhookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

//Get payload from New Relic
//Route::post('/webhook', [WebhookController::class, 'index']);
Route::get('/mail', [WebhookController::class, 'sendMail']);

//Severity
Route::get('/severity', [\App\Http\Controllers\ZenossEventSeverityController::class, 'index']);

//URLPageViewQuery
Route::get('/urlpageviewquery', [\App\Http\Controllers\URLPageViewQueryController::class, 'index']);

//Description_field
Route::get('/descriptionfiels', [\App\Http\Controllers\DescriptionFieldController::class, 'index']);

//Templates
Route::get('/templates', [\App\Http\Controllers\TemplateController::class, 'index']);
Route::get('/templates/create', [\App\Http\Controllers\TemplateController::class, 'create']);
Route::post('/templates', [\App\Http\Controllers\TemplateController::class, 'store']);
Route::get('/templates/{id}', [\App\Http\Controllers\TemplateController::class, 'show']);
Route::get('/templates/{id}/edit', [\App\Http\Controllers\TemplateController::class, 'edit']);
Route::put('/templates/{id}', [\App\Http\Controllers\TemplateController::class, 'update']);
Route::delete('/templates/{id}', [\App\Http\Controllers\TemplateController::class, 'destroy']);

//Home
Route::post('/channel', [\App\Http\Controllers\HomeController::class, 'index']);

//Filter
Route::get('/filter', function () {
    return view('filter.index');
});
