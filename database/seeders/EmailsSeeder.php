<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emails')->insert([
            [
                'policy_name' => 'APM_O_W_RENAPO_G1',
                'address_to' => 'soporte-n1@triara.com, iris.lopez@enginecore.com.mx, operacion.itoc@triara.com, israel.domingo@enginecore.com.mx, fernando.isoard@enginecore.com.mx, eduardo.leyte@enginecore.com.mx',
                'address_cc' => 'humberto.lozada@triara.com, rodrigo.mendoza@triara.com, wilmer.sanchez@triara.com, itapia@segob.gob.mx, bgalicia@segob.gob.mx, magalvez@segob.gob.mx',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'policy_name' => 'APM_O_C_RENAPO_G1',
                'address_to' => 'soporte-n1@triara.com,iris.lopez@enginecore.com.mx,operacion.itoc@triara.com,israel.domingo@enginecore.com.mx,fernando.isoard@enginecore.com.mx, eduardo.leyte@enginecore.com.mx',
                'address_cc' => 'humberto.lozada@triara.com,rodrigo.mendoza@triara.com,wilmer.sanchez@triara.com,itapia@segob.gob.mx,bgalicia@segob.gob.mx,magalvez@segob.gob.mx',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'policy_name' => 'SYN_O_C_RENAPO_G1',
                'address_to' => 'soporte-n1@triara.com,iris.lopez@enginecore.com.mx,operacion.itoc@triara.com,bgalicia@segob.gob.mx,magalvez@segob.gob.mx,israel.domingo@enginecore.com.mx,fernando.isoard@enginecore.com.mx, eduardo.leyte@enginecore.com.mx',
                'address_cc' => 'humberto.lozada@triara.com,rodrigo.mendoza@triara.com,wilmer.sanchez@triara.com',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'policy_name' => 'APM_O_C_CHECKAPP_G1',
                'address_to' => 'operacion.itoc@triara.com',
                'address_cc' => 'humberto.lozada@triara.com, rodrigo.mendoza@triara.com, wilmer.sanchez@triara.com',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'policy_name' => 'APM_O_W_INEGI_G1',
                'address_to' => 'soporte-n1@triara.com',
                'address_cc' => 'operacion.itoc@triara.com,wilmer.sanchez@triara.com,eduardo.jimenez@triara.com,jose.martinezr@inegi.org.mx,humberto.lopez@inegi.org.mx,hilda.garcia@inegi.org.mx,leobardo.rodriguez@inegi.org.mx,juancarlos.cuenca@inegi.org.mx',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'policy_name' => 'BRO_O_W_INEGI_G1',
                'address_to' => 'soporte-n1@triara.com',
                'address_cc' => 'operacion.itoc@triara.com,wilmer.sanchez@triara.com,eduardo.jimenez@triara.com,jose.martinezr@inegi.org.mx,humberto.lopez@inegi.org.mx,hilda.garcia@inegi.org.mx,leobardo.rodriguez@inegi.org.mx,juancarlos.cuenca@inegi.org.mx',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'policy_name' => 'INFRA_O_W_INEGI_G1',
                'address_to' => 'soporte-n1@triara.com',
                'address_cc' => 'operacion.itoc@triara.com,wilmer.sanchez@triara.com,eduardo.jimenez@triara.com,jose.martinezr@inegi.org.mx,humberto.lopez@inegi.org.mx,hilda.garcia@inegi.org.mx,leobardo.rodriguez@inegi.org.mx,juancarlos.cuenca@inegi.org.mx',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'policy_name' => 'INFRA_O_W_INEGI_G2',
                'address_to' => 'soporte-n1@triara.com',
                'address_cc' => 'operacion.itoc@triara.com,wilmer.sanchez@triara.com,eduardo.jimenez@triara.com,jose.martinezr@inegi.org.mx,humberto.lopez@inegi.org.mx,hilda.garcia@inegi.org.mx,leobardo.rodriguez@inegi.org.mx,juancarlos.cuenca@inegi.org.mx,elsa.corona@inegi.org.mx,Juan.Reyes@inegi.org.mx',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'policy_name' => 'INFRA_O_W_INEGI_G3',
                'address_to' => 'soporte-n1@triara.com',
                'address_cc' => 'operacion.itoc@triara.com,wilmer.sanchez@triara.com,eduardo.jimenez@triara.com,jose.martinezr@inegi.org.mx,humberto.lopez@inegi.org.mx,hilda.garcia@inegi.org.mx,leobardo.rodriguez@inegi.org.mx,juancarlos.cuenca@inegi.org.mx,fernando.gallegos@inegi.org.mx',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
