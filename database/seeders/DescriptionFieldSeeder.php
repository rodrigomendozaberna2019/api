<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DescriptionFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('description_fields')->insert([
            [
                'name' => 'New Relic Alert - Test Condition',
                'description' => 'TEST Description',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'New Relic Alert - Test Policy',
                'description' => 'TEST Description',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Instances Error percentage (High)',
                'description' => 'Se ha detectado un incremento en el conteo de errores de la aplicación. Los errores no manejados son aquellos que interrumpen una transacción o generan un mensaje de excepción, como por ejemplo el codigo 500 en una transacción web.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Error percentage (High)',
                'description' => 'Se ha detectado un incremento en el conteo de errores de la aplicación. Los errores no manejados son aquellos que interrumpen una transacción o generan un mensaje de excepción, como por ejemplo el codigo 500 en una transacción web.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Apdex (Low)',
                'description' => 'Se ha detectado que un gran número de transacciones de la aplicación contienen error o degradación lo que puede estar impactando la experiencia del usuario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Response time (web) (High)',
                'description' => 'Se ha detectado lentitud en una o más transacciones web (aquellas que inician con una petición HTTP), esto es un indicativo de degradación que puede afectar tanto despligue de portales como la comunicación entre las APIs de los sistemas.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Response time (background) (High)',
                'description' => 'Se ha detectado lentitud en una o más transacciones non-web (aquellas que NO inician con una petición HTTP) como por ejemplo querys a bases de datos, colas de mensajeria, jobs de la aplicación o procesos de backgroud. La lentitud presentada por estas métricas puede afectar el desempeño de la aplicación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Heap memory usage (High)',
                'description' => 'Se ha detectado un alto consumo de memoria en la Máquina Virual de Java, el cual se ha mantenido por encima del umbral establecido durante mas de 5 minutos.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'CPU utilization time (High)',
                'description' => 'Se ha detectado un alto consumo de CPU en la maquina VIirtual de Java, el cual se ha mantenido por encima del umbral establecido durante mas de 5 minutos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Check Failure',
                'description' => 'Se ha detectada un fallo en el check de la URL o script sintético',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Rate Query Baseline',
                'description' => 'Se ha detectado un compartamiento inusualmente bajo en el conteo de accesos a este enlace',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Datastore/all (Baseline)',
                'description' => 'Se ha detectado un incremento en el tiempo de respuesta de la Base de Datos con respecto a la linea base de su comportamiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Key transactions - Response time (High)',
                'description' => 'Se ha detectado lentitud en una de las transacción clave',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'httpd down',
                'description' => 'Se ha detacato una caída en el servidor http de esta aplicación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Nginx down',
                'description' => 'Se ha detacato una caída en el servidor http de esta aplicación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Nginx Desbalanceados',
                'description' => 'Se ha detectado que los servidores http de esta aplicación no estan atendiendo las peticiones de forma balanceada',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Porcentaje de uso del CPU',
                'description' => 'Se ha detectado un alto consumo de CPU, el cual se ha mantenido por encima del umbral establecido durante mas de 5 minutos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Porcentaje de uso de disco',
                'description' => 'Se ha detectado un alto consumo de utización de disco, el cual se ha mantenido por encima del umbral establecido durante mas de 5 minutos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Porcentaje de memoria usada',
                'description' => 'Se ha detectado un alto consumo de memoria, el cual se ha mantenido por encima del umbral establecido durante mas de 5 minutos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Disponibilidad de host',
                'description' => 'Se ha detectado indisponibilidad en el host durante mas de 5 minutos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Page view load time (Baseline)',
                'description' => 'Se ha detectado que el tiempo de respuesta de la página ha sufrido una desviación por encima de su linea base, lo que significa que los usuarios estan percibiendo lentitud en el servicio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'NRQL Response time',
                'description' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
