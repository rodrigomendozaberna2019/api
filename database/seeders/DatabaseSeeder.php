<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(URLPageViewQuerySeeder::class);
        $this->call(ZenossEventSeveritySeeder::class);
        $this->call(DescriptionFieldSeeder::class);
        $this->call(HelpNotesFieldSeeder::class);
        $this->call(TemplateSeeder::class);
        $this->call(EmailsSeeder::class);
    }
}
