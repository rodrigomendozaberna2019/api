<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HelpNotesFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('help_notes_fields')->insert([
            [
                'name' => 'Instances Error percentage (High)',
                'description' => 'Ir a la URL del incidente, una vez deplegada la grafica de la alerta, hacer click en el enlace "Go to application overview" el cual desplegara la vista general de la aplicacion. En el menu de la izquierda ir a "Error Analytics" en la sección "Events" y revisar el detalle del error mas frecuente',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Error percentage (High)',
                'description' => 'Ir a la URL del incidente, una vez deplegada la grafica de la alerta, hacer click en el enlace "Go to application overview" el cual desplegara la vista general de la aplicacion. En el menu de la izquierda ir a "Error Analytics" en la sección "Events" y revisar el detalle del error mas frecuente',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Apdex (Low)',
                'description' => 'Ir a la URL del incidente, una vez deplegada la grafica de la alerta, hacer click en el enlace "Go to application overview" el cual desplegara la vista general de la aplicacion. Revisar los tiempos de respuesta en la grafica "Web Transaction Time". Para verificar las transacciones web más lentas ir al menu "Transacctions" ordenar por transacciones web y seleccionar "Apdex most dissatisfying", analizar las que presenten mayor porcentaje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Response time (web) (High)',
                'description' => 'Ir a la URL del incidente, una vez deplegada la gráfica de la alerta, hacer click en el enlace "Go to application overview" el cual desplegará la vista general de la aplicación. Revisar los tiempos de respuesta en la grafica "Web Transaction Time". Para verificar las transacciones web más lentas ir al menú "Transacctions" ordenar por transacciones web y seleccionar "Most Time Consuming", analizar las que presenten mayor porcentaje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Response time (background) (High)',
                'description' => 'Ir a la URL del incidente, una vez deplegada la gráfica de la alerta, hacer click en el enlace "Go to application overview" el cual desplegará la vista general de la aplicación. Revisar los tiempos de respuesta en la grafica "Non-Web Transaction Time". Para verificar las transacciones más lentas, ir al menú "Transacctions" ordenar por transacciones Non-Web y seleccionar "Most Time Consuming", analizar las que presenten mayor porcentaje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Heap memory usage (High)',
                'description' => 'Ir a la URL del incidente, una vez deplegada la gráfica de la alerta, hacer click en el enlace "Go to application overview" el cual desplegará la seccion JVMs donde se puede consultar la gráfica "Heap Memory usage (MB)"',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'CPU utilization time (High)',
                'description' => 'Ir a la URL del incidente, una vez deplegada la gráfica de la alerta, hacer click en el enlace "Go to application overview" el cual desplegará la vista general de la aplicación. Para ver el detalle de la máquina Virtual de Java ir al JVMs en la sección "Monitoring"',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'NRQL Response time',
                'description' => 'Acceder a la URL del incidente y analizar el comportamiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Rate Query Baseline',
                'description' => 'Acceder a la URL del incidente y analizar el comportamiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Key transactions - Response time (High)',
                'description' => 'Ir a la URL del incidente, una vez deplegada la gráfica de la alerta, hacer click en el enlace "Go to ... overview" el cual redireccionará al dashboard de la transacción clave. Analizar el tiempo de respuesta.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Check Failure',
                'description' => 'Ir a la URL del incidente, una vez deplegada la tabla de resultados, hacer click en el enlace "Go to..." el cual mostrará el error que el script sintético ha detectado. Para validar si la página continua presentando errores se puede usar la opción "Re-check".',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Porcentaje de uso del CPU',
                'description' => 'Ir a la URL del incidente, una vez deplegada la gráfica de la alerta hacer click en el enlace del host alarmado, buscar en la nueva ventana la pestaña de procesos, analizar el que más cunsumo de CPU presente.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Porcentaje de uso de disco',
                'description' => 'Ir a la URL del incidente, una vez deplegada la gráfica de la alerta hacer click en el enlace del host alarmado, buscar en la nueva ventana la pestaña de Storage, analizar el consumo de los discos.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Porcentaje de memoria usada',
                'description' => 'Ir a la URL del incidente, una vez deplegada la gráfica de la alerta hacer click en el enlace del host alarmado, buscar en la nueva ventana la pestaña de procesos, analizar el que más cunsumo de memoria presente (MEMORY RESIDENT SIZE)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Disponibilidad de host',
                'description' => 'Ir a New Relic, en la sección de INFRASTRUCTURE buscar el host alarmado, verificar que reporte datos.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Page view load time (Baseline)',
                'description' => 'Ir a la URL del incidente, hacer click en el sitio alarmado, en la grafica Principal cambiar de SPA a PageView Load Time, analizar el componente que mas tiempo aporte al total',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'New Relic Alert - Test Condition',
                'description' => 'TEST help note',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'New Relic Alert - Test Policy',
                'description' => 'TEST help note',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
