<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('templates')->insert([
            [
                'client' => 'RENAPO',
                'type' => 'Application',
                'detail' => 'WSeCurp (apppr01wcurp:7004)',
                'police' => 'APM_O_W_RENAPO_G1',
                'data' => '
                {
                "summary": "",
                "severity" : "",
                "component":"",
                "closeKey":"",
                "state":"",
                "eventClassKey":"",
                "NR_aplication":"",
                "NR_description":"",
                "NR_policy":"",
                "NR_condition":"",
                "NR_incidentURL":"",
                "NR_detail":"",
                "NR_helpNotes":"",
                "NR_operationNotes":"",
                "NR_responsableGroup":"",
                "NR_clientGroup":"",
                "resource":""
                }',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'client' => 'INEGI',
                'type' => 'Infra',
                'detail' => 'iktan-wls-2',
                'police' => 'INFRA_O_W_INEGI_G3',
                'data' => '
                {
                "summary": "summary",
                "severity" : "severity",
                "component":"component",
                "closeKey":"closeKey",
                "state":"",
                "eventClassKey":"",
                "NR_aplication":"",
                "NR_description":"",
                "NR_policy":"",
                "NR_condition":"",
                "NR_incidentURL":"",
                "NR_detail":"",
                "NR_helpNotes":"",
                "NR_operationNotes":"",
                "NR_responsableGroup":"",
                "NR_clientGroup":"",
                "resource":""
                }',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
