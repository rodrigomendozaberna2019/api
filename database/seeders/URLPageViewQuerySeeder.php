<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class URLPageViewQuerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('url_page_view_queries')->insert([
            [
                'name' => 'telmex.com',
                'url' => 'https://telmex.com/',
                'description' => 'la página de Home de telmex',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'paquetes-de-internet-con-netflix',
                'url' => 'https://telmex.com/web/hogar/paquetes-de-internet-con-netflix',
                'description' => 'la página de Paquetes de Internet más Netflix',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'internet-sin-telefono-con-netflix',
                'url' => 'https://telmex.com/web/hogar/internet-sin-telefono-con-netflix',
                'description' => 'la página de Infinitum Puro más Netflix',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'aumenta-tu-velocidad',
                'url' => 'https://telmex.com/web/hogar/aumenta-tu-velocidad',
                'description' => 'la página de Upsell',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'paquetes-de-internet',
                'url' => 'https://telmex.com/web/hogar/paquetes-de-internet',
                'description' => 'la página de Paquetes de internet',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'internet-sin-telefono',
                'url' => 'https://telmex.com/web/hogar/internet-sin-telefono',
                'description' => 'la página de Infinitum Puro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'infinitum-bajo-demanda',
                'url' => 'https://telmex.com/web/hogar/infinitum-bajo-demanda',
                'description' => 'la página de Infinitum Bajo Demanda',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'paquetes-infinitum-negocio',
                'url' => 'https://telmex.com/web/negocios/paquetes-infinitum-negocio',
                'description' => 'la página de Paquetes Infinitum Negocio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'solo-internet',
                'url' => 'https://telmex.com/web/negocios/solo-internet',
                'description' => 'la página de Infinitum Puro Negocio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
