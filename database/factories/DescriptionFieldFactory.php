<?php

namespace Database\Factories;

use App\Models\DescriptionField;
use Illuminate\Database\Eloquent\Factories\Factory;

class DescriptionFieldFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DescriptionField::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
