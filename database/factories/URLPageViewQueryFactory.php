<?php

namespace Database\Factories;

use App\Models\URLPageViewQuery;
use Illuminate\Database\Eloquent\Factories\Factory;

class URLPageViewQueryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = URLPageViewQuery::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
