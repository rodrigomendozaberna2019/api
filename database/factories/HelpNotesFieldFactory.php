<?php

namespace Database\Factories;

use App\Models\HelpNotesField;
use Illuminate\Database\Eloquent\Factories\Factory;

class HelpNotesFieldFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HelpNotesField::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
