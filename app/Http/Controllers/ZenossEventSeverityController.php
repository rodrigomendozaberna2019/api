<?php

namespace App\Http\Controllers;

use App\Models\ZenossEventSeverity;
use Illuminate\Http\Request;

class ZenossEventSeverityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ZenossEventSeverity::all();

        return json_decode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ZenossEventSeverity $zenossEventSeverity
     * @return \Illuminate\Http\Response
     */
    public function show(ZenossEventSeverity $zenossEventSeverity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ZenossEventSeverity $zenossEventSeverity
     * @return \Illuminate\Http\Response
     */
    public function edit(ZenossEventSeverity $zenossEventSeverity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ZenossEventSeverity $zenossEventSeverity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ZenossEventSeverity $zenossEventSeverity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ZenossEventSeverity $zenossEventSeverity
     * @return \Illuminate\Http\Response
     */
    public function destroy(ZenossEventSeverity $zenossEventSeverity)
    {
        //
    }
}
