<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $request->getContent();
        $data = $request->email;
        Session::push('mail', $data);
        $data = Session::get('mail');
        return View::make('home.index')->with('data', $data);
    }
}
