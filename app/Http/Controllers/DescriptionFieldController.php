<?php

namespace App\Http\Controllers;

use App\Models\DescriptionField;
use Illuminate\Http\Request;

class DescriptionFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $descriptionfiels = DescriptionField::all();

        return json_decode($descriptionfiels);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\DescriptionField $descriptionField
     * @return \Illuminate\Http\Response
     */
    public function show(DescriptionField $descriptionField)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\DescriptionField $descriptionField
     * @return \Illuminate\Http\Response
     */
    public function edit(DescriptionField $descriptionField)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DescriptionField $descriptionField
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DescriptionField $descriptionField)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\DescriptionField $descriptionField
     * @return \Illuminate\Http\Response
     */
    public function destroy(DescriptionField $descriptionField)
    {
        //
    }
}
