<?php

namespace App\Http\Controllers;

use App\Models\HelpNotesField;
use Illuminate\Http\Request;

class HelpNotesFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HelpNotesField  $helpNotesField
     * @return \Illuminate\Http\Response
     */
    public function show(HelpNotesField $helpNotesField)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HelpNotesField  $helpNotesField
     * @return \Illuminate\Http\Response
     */
    public function edit(HelpNotesField $helpNotesField)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HelpNotesField  $helpNotesField
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HelpNotesField $helpNotesField)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HelpNotesField  $helpNotesField
     * @return \Illuminate\Http\Response
     */
    public function destroy(HelpNotesField $helpNotesField)
    {
        //
    }
}
