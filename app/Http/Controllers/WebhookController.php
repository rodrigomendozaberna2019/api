<?php

namespace App\Http\Controllers;

use App\Models\DescriptionField;
use App\Models\Emails;
use App\Models\HelpNotesField;
use App\Models\Template;
use App\Models\URLPageViewQuery;
use App\Models\ZenossEventSeverity;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class WebhookController extends Controller
{
    //

    /**
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $jsonString = $request->getContent();
        $data = json_decode($jsonString);
        return $this->createTemplate($data);
    }

    public function store(Request $request)
    {
        $jsonString = $request->getContent();
        $data = json_decode($jsonString);

        $average = array([
            'summary' => $data->targets[0]->name . ' - ' . $data->details,
            'severity' => $this->mapZenossEventSeverity($data),
            'component' => $data->policy_name . ' - ' . $data->condition_name,
            'closeKey' => $data->condition_id . $data->incident_id,
            'state' => $data->current_state,
            'eventClassKey' => $data->targets[0]->product,
            'NR_aplication' => $this->getURL_PageViewQuery($data),
            'NR_description' => $this->setDescriptionField($data),
            'NR_policy' => $data->policy_name,
            'NR_condition' => $data->condition_name,
            'NR_incidentURL' => $data->incident_url,
            'NR_detail' => $data->details,
            'NR_helpNotes' => $this->setHelpNotesField($data),
            'NR_operationNotes' => $this->setOperationNotesField($data),
            'NR_responsableGroup' => $this->setResponsableGroupField($data),
            'NR_clientGroup' => $this->setClientGroupField($data),
            'resource' => $this->setResourceZenoss($data),
        ]);
        Log::info($average);
        return $average;
    }

    public function createTemplate($data)
    {
        $average = collect([
            'summary' => $data->targets[0]->name . ' - ' . $data->details,
            'severity' => $this->mapZenossEventSeverity($data),
            'component' => $data->policy_name . ' - ' . $data->condition_name,
            'closeKey' => $data->condition_id . $data->incident_id,
            'state' => $data->current_state,
            'eventClassKey' => $data->targets[0]->product,
            'NR_aplication' => $this->getURL_PageViewQuery($data),
            'NR_description' => $this->setDescriptionField($data),
            'NR_policy' => $data->policy_name,
            'NR_condition' => $data->condition_name,
            'NR_incidentURL' => $data->incident_url,
            'NR_detail' => $data->details,
            'NR_helpNotes' => $this->setHelpNotesField($data),
            'NR_operationNotes' => $this->setOperationNotesField($data),
            'NR_responsableGroup' => $this->setResponsableGroupField($data),
            'NR_clientGroup' => $this->setClientGroupField($data),
            'resource' => $this->setResourceZenoss($data),
        ]);

        //SELECCION DE APERTURA O CIERRE DEL EVENTO
        if ($data->current_state == 'open' or $data->current_state == 'test') {
            //$this->openZenossEvent($data);
            //$this->mailDistribution($data);
            $this->sendMail($data, $average);
        }
    }

    /**
     * @param $data
     * @return string
     * Mapea la severidad y regresa la descripción correspondiente
     */
    public function mapZenossEventSeverity($data)
    {
        //Get all type severity from database
        $severity = ZenossEventSeverity::where('name', $data->severity)->get();

        if ($data->severity == 'CRITICAL') {
            if ($data->policy_name || '_W_') {
                return "Warning";
            }
        } else {
            Log::info('[mapZenossEventSeverity] Se ha mapeado la severidad como ' . $severity[0]->value);
            return $severity[0]->value;
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getURL_PageViewQuery($data)
    {
        //Get all URL_PageViewQuery from database
        $URL_PageViewQuery = URLPageViewQuery::where('name', $data->condition_name)->get();

        if ($data->targets[0]->name == 'PageView query') {
            return json_decode($URL_PageViewQuery);
        } else {
            return $data->targets[0]->name;
        }

    }

    /**
     * @param $data
     * @return mixed|null
     */
    public function setDescriptionField($data)
    {
        $pageUrl = $this->getURL_PageViewQuery($data);
        $account = $data->account_id;
        $QueryKey = $this->getQueryKey($data);

        //Get all Description Fields
        if ($data->account_id == '2530960' and $data->condition_name >= 'NRQL Response time') {
            $response = Http::withHeaders([
                'X-Query-Key' => $QueryKey
            ])->get("https://insights-api.newrelic.com/v1/accounts/" . $account . "/query?nrql=FROM PageView SELECT average(duration) WHERE pageUrl = '" . $pageUrl . "' since 15 days ago UNTIL 5 day ago");
            Log::info($response);
        } else {
            $DescriptionField = DescriptionField::where('name', $data->condition_name)->get();
            return $DescriptionField[0]->description ?? 'No se a asignado una descripción';
        }

    }

    /**
     * @param $data
     * @return mixed
     */
    public function setHelpNotesField($data)
    {
        $HelpNotesField = HelpNotesField::where('name', $data->condition_name)->get();
        return $HelpNotesField[0]->description ?? 'No se a asignado una nota';
    }

    /**
     * @param $data
     * @return string
     */
    public function getQueryKey($data)
    {

        if ($data->account_id == '3011621') { #RENAPO
            return '';
        } elseif ($data->account_id == '2623926') { #INEGI
            return '';
        } elseif ($data->account_id == '2530960') {
            return 'NRIQ-Vd-24DYWLdxsFpKOiBaf6LD_2o-o3lmf'; # TELMEX
        } else {
            return 'No definida';
        }
    }

    /**
     * @param $data
     * @return string
     */
    public function setOperationNotesField($data)
    {
        $foo = $data->account_id;
        if ($foo == '3011621') {
            return 'Favor de generar un IM a la torre de Operaciones RENAPO App para su atención.';
        } else {
            return 'No hay observaciones especiales para este tipo evento, favor seguir con el procedimiento regular';
        }
    }

    /**
     * @param $data
     * @return string
     */
    public function setResponsableGroupField($data)
    {
        $foo = $data->account_id;

        if ($foo == '3011621') {
            return 'Engine Core Aplicativos';
        } elseif ($foo == '2623926') {
            return 'Soporte INEGI';
        } else {
            return 'No definida';
        }
    }


    /**
     * @param $data
     * @return string
     */
    public function setClientGroupField($data)
    {
        $foo = $data->account_id;

        if ($foo == '3011621') {
            return 'GOBIERNO / RENAPO';
        } elseif ($foo == '2623926') {
            return 'GOBIERNO / INEGI';
        } else {
            return 'No definida';
        }
    }

    /**
     * @param $data
     * @return string
     * RESOURCE ES USADO PARA EL TRIGGER DE ZENOSS
     */
    public function setResourceZenoss($data)
    {
        if ($data->policy_name == 'TEST') {
            return 'New Relic - TEST';
        } else {
            return 'New Relic - ' . $data->policy_name;
        }
    }

    public function openZenossEvent($data)
    {
        return $data;
    }

    public function fillTemplate($data, $average)
    {
        date_default_timezone_set("America/Mexico_City");
        $hora_event = date("d-m-Y h:i:s");

        if ($data->policy_name >= 'INFRA_') {
            $template = file_get_contents('../resources/views/mail/Mail_Template_infra.html');
        } else {
            $template = file_get_contents('../resources/views/mail/Mail_Template.html');
        }

        $template = str_replace('${evt/NR_clientGroup}', $average['NR_clientGroup'], $template);
        $template = str_replace('${evt/NR_aplication}', $average['NR_aplication'], $template);
        $template = str_replace('${evt/NR_description}', $average['NR_description'], $template);
        $template = str_replace('${evt/NR_policy}', $average['NR_policy'], $template);
        $template = str_replace('${evt/NR_condition}', $average['NR_condition'], $template);
        $template = str_replace('${evt/NR_detail}', $average['NR_detail'], $template);
        $template = str_replace('${evt/NR_incidentURL}', $average['NR_incidentURL'], $template);
        $template = str_replace('${evt/severityString}', $average['severity'], $template);
        $template = str_replace('${evt/NR_responsableGroup}', $average['NR_responsableGroup'], $template);
        $template = str_replace('${evt/NR_seguimiento}', 'Informar al grupo responsable', $template);
        $template = str_replace('${evt/firstTime}', $hora_event, $template);
        $template = str_replace('${evt/NR_helpNotes}', $average['NR_helpNotes'], $template);
        $template = str_replace('${evt/NR_operationNotes}', $average['NR_operationNotes'], $template);

        return $template;

    }

    public function sendMail($data, $average)
    {
        $subject = 'PHP TEST -' . $average['NR_condition'] . ' - ' . $average['severity'] . ' - Monitoreo Proactivo ITOC-A - ' . $average['NR_clientGroup'] . ' - New Relic';

        $emails = Emails::where('policy_name', $data->policy_name)->get();

        $client = new Client(['base_uri' => 'https://itoc-api.triara.com:8088/']);
        //$client = new Client(['base_uri' => 'https://reqres.in/']);

        $response = $client->request('POST', '/notificacion/mail/solotext', ['form_params' => [
            //$response = $client->request('POST', '/api/users', ['form_params' => [
            'subjetc' => $subject,
            'from' => 'apm.itoc@triara.com',
            'mensaje' => html_entity_decode($this->fillTemplate($data, $average)),
            //'grupo_envio' => $emails[0]->address_to,
            'grupo_envio' => 'rodrigo.mendoza@triara.com, wilmer.sanchez@triara.com, carlos.carmona@triara.com, david.gomez@triara.com',
        ]]);

        Log::info($response->getBody());

        return $response->getStatusCode();

    }
}
