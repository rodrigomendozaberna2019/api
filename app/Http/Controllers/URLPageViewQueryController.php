<?php

namespace App\Http\Controllers;

use App\Models\URLPageViewQuery;
use Illuminate\Http\Request;

class URLPageViewQueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $URLPageViewQuery = URLPageViewQuery::all();

        return json_decode($URLPageViewQuery);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\URLPageViewQuery $uRLPageViewQuery
     * @return \Illuminate\Http\Response
     */
    public function show(URLPageViewQuery $uRLPageViewQuery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\URLPageViewQuery $uRLPageViewQuery
     * @return \Illuminate\Http\Response
     */
    public function edit(URLPageViewQuery $uRLPageViewQuery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\URLPageViewQuery $uRLPageViewQuery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, URLPageViewQuery $uRLPageViewQuery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\URLPageViewQuery $uRLPageViewQuery
     * @return \Illuminate\Http\Response
     */
    public function destroy(URLPageViewQuery $uRLPageViewQuery)
    {
        //
    }
}
