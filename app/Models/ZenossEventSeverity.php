<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZenossEventSeverity extends Model
{
    use HasFactory;

    protected $table = 'zenoss_event_severities';
}
