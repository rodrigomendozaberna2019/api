<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DescriptionField extends Model
{
    use HasFactory;

    protected $table = 'description_fields';
}
