<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class URLPageViewQuery extends Model
{
    use HasFactory;

    protected $table = 'url_page_view_queries';
}
