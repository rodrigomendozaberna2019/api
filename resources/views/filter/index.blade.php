@extends('layouts.master')

@section('content')
    <div>
        <h1>Filter</h1>

        <div class="grid grid-cols-3">
            <div>
                <input type="text" name="campo" id="campo">
            </div>
            <div>
                <select name="operador" id="operador">
                    <option value="uno">equals</option>
                    <option value="dos">container</option>
                </select>
            </div>
            <div>
                <input type="text" name="valor" id="valor">
            </div>
        </div>
    </div>
@stop

