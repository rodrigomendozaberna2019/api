<!-- component -->
<div class="flex flex-wrap bg-gray-100 w-full h-screen">
    <div class="w-3/12 bg-white rounded p-3 shadow-lg">
        <div class="flex items-center space-x-4 p-2 mb-5">
            <div>
                <h4 class="font-bold text-lg text-gray-700 capitalize font-poppins tracking-wide">ITOC Enriquece</h4>
            </div>
        </div>
        <ul class="space-y-2 text-sm">
            <li>
                <a href="{{url('/')}}"
                   class="flex items-center space-x-3 text-gray-700 p-2 rounded-md font-medium hover:bg-gray-200 {{request()->is('/') ? 'bg-gray-200' : null}} focus:shadow-outline">
                    <span class="text-gray-600">
                        <svg class="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                             stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                  d="M9.75 17L9 20l-1 1h8l-1-1-.75-3M3 13h18M5 17h14a2 2 0 002-2V5a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"/>
                        </svg>
                    </span>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/templates') }}"
                   class="flex items-center space-x-3 text-gray-700 p-2 rounded-md font-medium hover:bg-gray-200 {{request()->is('templates') ? 'bg-gray-200' : null}} focus:shadow-outline">
                    <span class="text-gray-600">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                             stroke="currentColor">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"/>
                        </svg>
                    </span>
                    <span>Plantillas</span>
                </a>
            </li>
        </ul>
    </div>

    <div class="w-9/12">
        <div class="p-4 text-gray-500">
            @yield('content')
        </div>
    </div>
</div>
