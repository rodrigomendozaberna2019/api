@extends('layouts.master')

@section('content')
    <div>
        <div class="py-4">
            <h1 class="font-bold text-black text-2xl">Editar plantilla</h1>
        </div>
        <div>
            <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                <div class="px-4 py-10 sm:px-6 bg-blue-100">
                    <h3 class="text-lg leading-6 font-bold text-blue-900">
                        Centro Integral de Monitoreo y Gestión ITOC
                    </h3>
                </div>
                <form>
                    <div class="border-t border-gray-200">
                        <dl>
                            <div class="bg-blue-50 px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-indigo-900">
                                    Empresa Afectada:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$template->client}}
                                </dd>
                            </div>
                            <div class="bg-white px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                @if($template->type == 'Host')
                                    <dt class="text-sm font-bold text-black-900">Host:</dt>
                                @else
                                    <dt class="text-sm font-bold text-black-900">Aplicación:</dt>
                                @endif
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$template->detail}}
                                </dd>
                            </div>
                            <div class="bg-blue-50 px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-indigo-900">
                                    Descripción:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    <div class="mt-1">
                                    <textarea id="about" name="about" rows="3"
                                              class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                              placeholder="Ingresa una descripción">{{$data['NR_description']}}</textarea>
                                    </div>
                                </dd>
                            </div>
                            <div class="bg-white px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-black-900">
                                    Política y condición:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$data['NR_policy']}}
                                </dd>
                            </div>
                            <div class="bg-blue-50 px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-indigo-900">
                                    URL del incidente:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$data['NR_incidentURL']}}
                                </dd>
                            </div>
                            <div class="bg-white px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-black-900">
                                    Detalle:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$data['NR_detail']}}
                                </dd>
                            </div>
                            <div class="bg-blue-50 px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-indigo-900">
                                    Tipo de Evento:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$data['severity']}}
                                </dd>
                            </div>
                            <div class="bg-white px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-black-900">
                                    Grupo responsable:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$data['NR_responsableGroup']}}
                                </dd>
                            </div>
                            <div class="bg-blue-50 px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-indigo-900">
                                    Seguimiento:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    Informar al grupo responsable
                                </dd>
                            </div>
                            <div class="bg-white px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-black-900">
                                    Hora de inicio:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    Excepteur qui ipsum aliquip consequat sint.
                                </dd>
                            </div>
                            <div class="bg-blue-50 px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-indigo-900">Recomendaciones para la revisión del
                                    evento:
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    <textarea id="about" name="about" rows="3"
                                              class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                              placeholder="Ingresa una descripción">{{$data['NR_helpNotes']}}</textarea>

                                </dd>
                            </div>
                            <div class="bg-white px-4 py-2 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-bold text-black-900">Observaciones de notificación:</dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    <textarea id="about" name="about" rows="3"
                                              class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                              placeholder="Ingresa una descripción">{{$data['NR_operationNotes']}}</textarea>
                            </div>
                            </dd>
                    </div>
                    </dl>
            </div>

            </form>
        </div>
    </div>
    </div>
@stop
