<html>
<head/>
<body><img style="float: left;width: 538px; height: 119px;"
           src="http://200.57.138.134/ITOC/images/bannerItoc.jpg"/><br/>
<table width="100%">
    <tr style="background-color: #F0F8FF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000080; text-align: right; font-weight: 700; width: 16%;">
            Empresa Afectada:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            {{$json['NR_clientGroup']}}
        </td>
    </tr>
    <tr style="background-color: #FFFFFF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: right; font-weight: 700; width: 16%;">
            Aplicación
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            {{$json['NR_aplication']}}
        </td>
    </tr>
    <tr style="background-color: #F0F8FF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000080; text-align: right; font-weight: 700; width: 16%;">
            Descripción:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            {{$json['NR_description']}}
        </td>
    </tr>
    <tr style="background-color: #FFFFFF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: right; font-weight: 700; width: 16%;">
            Política y condición:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            {{$json['NR_policy']}} - {{$json['NR_condition']}}
        </td>
    </tr>
    <tr style="background-color: #F0F8FF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000080; text-align: right; font-weight: 700; width: 16%;">
            URL del incidente:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            {{$json['NR_incidentURL']}}
        </td>
    </tr>
    <tr style="background-color: #FFFFFF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: right; font-weight: 700; width: 16%;">
            Detalle:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            {{$json['NR_detail']}}
        </td>
    </tr>
    <tr style="background-color: #F0F8FF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000080; text-align: right; font-weight: 700; width: 16%;">
            Tipo de Evento:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            ${evt/severityString}
        </td>
    </tr>
    <tr style="background-color: #FFFFFF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: right; font-weight: 700; width: 16%;">
            Grupo Responsable:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            ${evt/NR_responsableGroup}
        </td>
    </tr>
    <tr style="background-color: #F0F8FF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000080; text-align: right; font-weight: 700; width: 16%;">
            Seguimiento:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            ${evt/NR_seguimiento}
        </td>
    </tr>
    <tr style="background-color: #FFFFFF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: right; font-weight: 700; width: 16%;">
            Hora de inicio:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            ${evt/firstTime}
        </td>
    </tr>
    <tr style="background-color: #F0F8FF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000080; text-align: right; font-weight: 700; width: 16%;">
            Recomendaciones para la revisión del evento:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            ${evt/NR_helpNotes}
        </td>
    </tr>
    <tr style="background-color: #FFFFFF;">
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: right; font-weight: 700; width: 16%;">
            Observaciones de notificación:
        </td>
        <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 83%;">
            ${evt/NR_operationNotes}
        </td>
    </tr>
    <tr style="background-color: #F0F8FF;">
</table>
<br/>
</body>
</html>
